package org.jcnc.jnotepad.app.common.constants;

/**
 * 文本常量类，包含多处使用的文本常量。
 *
 * <p>如果只有一个类使用常量，请在该类中使用 <code>private static final</code> 声明。</p>
 *
 * @author gewuyou
 */
public class TextConstants {

    /**
     * 标题文本常量
     */
    public static final String TITLE = "title";

    /**
     * 保存文本常量
     */
    public static final String SAVE = "SAVE";

    /**
     * 文件文本常量
     */
    public static final String FILE = "FILE";
    /**
     * 文件夹
     */
    public static final String FOLDER = "FOLDER";

    /**
     * 构建文本常量
     */
    public static final String BUILD = "BUILD";

    /**
     * 终端文本常量
     */
    public static final String TERMINAL = "TERMINAL";

    /**
     * 运行文本常量
     */
    public static final String RUN = "RUN";

    /**
     * 调试文本常量
     */
    public static final String DE_BUG = "DE_BUG";

    /**
     * 新建文本常量
     */
    public static final String NEW = "NEW";

    /**
     * 打开文本常量
     */
    public static final String OPEN = "OPEN";

    /**
     * 打开目录文本常量
     */
    public static final String OPEN_DIRECTORY = "OPEN_DIRECTORY";

    /**
     * 另存为文本常量
     */
    public static final String SAVE_AS = "SAVE_AS";

    /**
     * 重命名文本常量
     */
    public static final String RENAME = "RENAME";

    /**
     * 设置文本常量
     */
    public static final String SET = "SET";

    /**
     * 帮助文本常量
     */
    public static final String HELP = "HELP";

    /**
     * 自动换行文本常量
     */
    public static final String WORD_WRAP = "WORD_WRAP";

    /**
     * 插件文本常量
     */
    public static final String PLUGIN = "PLUGIN";

    /**
     * 管理插件文本常量
     */
    public static final String MANAGER_PLUGIN = "MANAGER_PLUGIN";

    /**
     * 关于文本常量
     */
    public static final String ABOUT = "ABOUT";

    /**
     * 开发者文本常量
     */
    public static final String DEVELOPER = "DEVELOPER";

    /**
     * 统计文本常量
     */
    public static final String STATISTICS = "STATISTICS";

    /**
     * 打开配置文件文本常量
     */
    public static final String OPEN_CONFIGURATION_FILE = "OPEN_CONFIGURATION_FILE";

    /**
     * 顶部文本常量
     */
    public static final String TOP = "TOP";

    /**
     * 语言文本常量
     */
    public static final String LANGUAGE = "LANGUAGE";

    /**
     * 中文文本常量
     */
    public static final String UPPER_CHINESE = "CHINESE";

    /**
     * 英文文本常量
     */
    public static final String UPPER_ENGLISH = "ENGLISH";

    /**
     * 新建文件文本常量
     */
    public static final String NEW_FILE = "NEW_FILE";

    /**
     * 新建文件夹
     */
    public static final String NEW_DIRECTORY = "NEW_DIRECTORY";

    /**
     * 删除
     */
    public static final String DELETE = "DELETE";

    /**
     * 行文本常量
     */
    public static final String ROW = "ROW";

    /**
     * 列文本常量
     */
    public static final String COLUMN = "COLUMN";

    /**
     * 字数统计文本常量
     */
    public static final String WORD_COUNT = "WORD_COUNT";

    /**
     * 编码文本常量
     */
    public static final String ENCODE = "ENCODE";

    /**
     * 英文小写文本常量
     */
    public static final String ENGLISH = "english";

    /**
     * 中文小写文本常量
     */
    public static final String CHINESE = "chinese";

    /**
     * 关闭
     */
    public static final String CLOSE = "CLOSE";

    /**
     * 关闭其他标签页
     */
    public static final String CLOSE_OTHER_TABS = "CLOSE_OTHER_TABS";

    /**
     * 关闭所有标签页
     */
    public static final String CLOSE_ALL_TABS = "CLOSE_ALL_TABS";

    /**
     * 关闭左侧标签
     */
    public static final String CLOSE_LEFT_TABS = "CLOSE_LEFT_TABS";

    /**
     * 关闭右侧标签
     */
    public static final String CLOSE_RIGHT_TABS = "CLOSE_RIGHT_TABS";

    /**
     * 复制
     */
    public static final String COPY = "COPY";

    /**
     * 粘贴
     */
    public static final String PASTE = "PASTE";

    /**
     * 剪切
     */
    public static final String SHEAR = "SHEAR";

    /**
     * 文件名
     */
    public static final String FILE_NAME = "FILE_NAME";

    /**
     * 文件路径
     */
    public static final String FILE_PATH = "FILE_PATH";


    /**
     * 所在文件夹
     */
    public static final String FOLDER_PATH = "FOLDER_PATH";
    /**
     * 固定标签页
     */
    public static final String FIXED_TAB = "FIXED_TAB";
    /**
     * 只读
     */
    public static final String READ_ONLY = "READ_ONLY";

    public static final String SEPARATOR = "separator";

    /**
     * 打开于
     */
    public static final String OPEN_ON = "OPEN_ON";
    /**
     * 资源管理器
     */
    public static final String EXPLORER = "EXPLORER";

    private TextConstants() {
    }

}
